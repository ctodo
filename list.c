#include <list.h>
#include <sqlite3.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

enum {
    LIST_DEFAULT = 0,
    LIST_ALL,
    LIST_DONE
};

void list_help() {
    printf("  Usage: list [all | done]\n");
    printf("  Lists all the tasks and their children.\n");
    printf("  Optional arguments:\n");
    printf("    all     lists all the tasks, including the ones that are completed\n");
    printf("    done    lists all the tasks that are completed (note that this results in a flat list)\n");
}

int print_children(sqlite3 *db, int parent, int depth, int which) {
    sqlite3_stmt *statement;
    char *query;
    char *notdone_expression = "AND done IS NULL";
    char *done_expression = "AND done NOT NULL";
    char *which_expression = NULL;
    int retVal;

    if (which == LIST_DEFAULT) {
        which_expression = notdone_expression;
    } else if (which == LIST_DONE) {
        which_expression = done_expression;
    }

    if (parent == -1) {
        if (which == LIST_DONE) {
            query = sqlite3_mprintf("SELECT * FROM todo WHERE done NOT NULL;");
        } else {
            query = sqlite3_mprintf("SELECT * FROM todo WHERE PARENT IS NULL %s ORDER BY key ASC;", which_expression);
        }
    } else {
        query = sqlite3_mprintf("SELECT * FROM todo WHERE parent = %d %s ORDER BY key ASC;", parent, which_expression);
    }

    retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    if (retVal != SQLITE_OK) {
        printf("An error occurred while trying to fetch the todo's from the database: %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(statement);
        return 1;
    }

    while ((retVal = sqlite3_step(statement)) == SQLITE_ROW) {
        time_t startDate, dueDate;
        const unsigned char *hasDueDate = sqlite3_column_text(statement, 3);
        startDate = (time_t) sqlite3_column_int(statement, 2);
        printf("%-*sTodo #%d: %s\n", depth*2, "", sqlite3_column_int(statement, 0), sqlite3_column_text(statement, 1));
        printf("%-*sCreated on: %s", depth*2, "", ctime(&startDate));
        if (hasDueDate) {
            dueDate = (time_t) sqlite3_column_int(statement, 3);
            printf("%-*sDue on: %s", depth*2, "", ctime(&dueDate));
        } else {
            printf("%-*sNo due date.\n", depth*2, "");
        }
        if (which == LIST_ALL) {
            printf("%-*sDone: %s.\n", depth*2, "", sqlite3_column_int(statement, 4) == 0 ? "no" : "yes");
        }
        printf("\n");
        if (which != LIST_DONE) {
            print_children(db, sqlite3_column_int(statement, 0), depth+1, which);
        }
    }

    sqlite3_free(query);
    sqlite3_finalize(statement);

    return 0;
}

int list(sqlite3 *db, MArray tokens) {
    int retVal;

    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        list_help();
        return 0;
    } else if (tokens->len == 1) {
        retVal = print_children(db, -1, 0, LIST_DEFAULT);
    } else if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "all")) {
        retVal = print_children(db, -1, 0, LIST_ALL);
    } else if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "done")) {
        retVal = print_children(db, -1, 0, LIST_DONE);
    } else {
        printf("Invalid amount of arguments.\n");
        list_help();
        return 0;
    }

    return retVal;
}
