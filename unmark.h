#ifndef __TODO_UNMARK_H
#define __TODO_UNMARK_H

#include <myarray.h>
#include <sqlite3.h>

int unmark(sqlite3 *db, MArray tokens);

#endif
