#include <add.h>
#include <stdio.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void add_help() {
    printf("  Usage: add [PARENT]\n");
    printf("  Adds a new task.\n");
    printf("  If you specify a PARENT, the new task will be a subtask of PARENT.\n");
}

char *escape(char *string) {
    int length = strlen(string);
    char *escaped = malloc((length+1)*sizeof(char));
    int i, j;

    for (i = 0, j = 0; string[i] != '\0'; i++, j++) {
        if (string[i] == '\'') {
            length++;
            escaped = realloc(escaped, (length+1)*sizeof(char));
            escaped[j] = '\'';
            j++;
        }
        escaped[j] = string[i];
    }

    escaped[j] = '\0';

    return escaped;
}

int add(sqlite3 *db, MArray tokens) {
    int parent = -1;
    int valid_parent;
    int parsed;
    char *description;
    char *escapedDescription;
    char *query;
    char *errmsg;
    sqlite3_stmt *statement;

    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        add_help();
        return 0;
    } else if (tokens->len == 2) {
        if (sscanf(myarray_get(tokens, char *, 1), "%d%n", &parent, &parsed) < 1 || parsed != strlen(myarray_get(tokens, char *, 1)) ||  parent < 0) {
            printf("Invalid argument. PARENT must be a positive integer.\n");
            add_help();
            return 0;
        }

        query = sqlite3_mprintf("SELECT COUNT(key) FROM todo WHERE key = %d;", parent);
        if (sqlite3_prepare_v2(db, query, -1, &statement, NULL) != SQLITE_OK) {
            printf("Couldn't prepare for the query that checks that a valid parent has been selected.\n");
            sqlite3_free(query);
            return 1;
        }
        while (sqlite3_step(statement) == SQLITE_ROW) {
            valid_parent = sqlite3_column_int(statement, 0);
        }
        sqlite3_finalize(statement);
        sqlite3_free(query);

        if (!valid_parent) {
            printf("The parent for which you want to create a subtask does not exist.\n");
            return 0;
        }
    } else if (tokens->len > 2) {
        printf("Invalid amount of arguments.\n");
        add_help();
        return 0;
    }

    description = readline("Please enter a description for the new task:\n");
    if (!description || strlen(description) <= 0) {
        printf("Your description must not be an empty string.\n");
        if (description) free(description);
        return 0;
    }

    escapedDescription = escape(description);
    free(description);

    if (parent != -1) {
        query = sqlite3_mprintf("INSERT INTO todo (description, createdDate, parent) VALUES (\'%s\', %d, %d);", escapedDescription, time(NULL), parent);
    } else {
        query = sqlite3_mprintf("INSERT INTO todo (description, createdDate) VALUES (\'%s\', %d);", escapedDescription, time(NULL));
    }

    if (sqlite3_exec(db, query, NULL, NULL, &errmsg) != SQLITE_OK) {
        printf("An error occurred while trying to add a new task: %s\n", errmsg);
    }
    sqlite3_free(errmsg);
    sqlite3_free(query);

    return 0;
}
