#ifndef __TODO_EXIT_H
#define __TODO_EXIT_H

#include <myarray.h>
#include <sqlite3.h>

int quit(sqlite3 *db, MArray tokens);

#endif
