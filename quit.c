#include <quit.h>
#include <stdio.h>
#include <string.h>

void quit_help() {
    printf("  Usage: quit\n");
    printf("  Exits the program.\n");
}

int quit(sqlite3 *db, MArray tokens) {
    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        quit_help();
        return 0;
    } else if (tokens->len != 1) {
        printf("Invalid amount of arguments.\n");
        quit_help();
        return 0;
    }

    return 1;
}
