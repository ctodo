#include <del.h>
#include <stdio.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <string.h>

void del_help() {
    printf("  Usage: del TODO#\n");
}

int del(sqlite3 *db, MArray tokens) {
    char *query;
    char *confirmation;
    char *errmsg;
    int todo;
    int retVal;
    int parsed;
    int hasChildren = 0;
    sqlite3_stmt *statement;

    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        del_help();
        return 0;
    } else if (tokens->len != 2) {
        printf("Invalid amount of arguments.\n");
        del_help();

        return 0;
    }

    if (sscanf(myarray_get(tokens, char *, 1), "%d%n", &todo, &parsed) < 1 || parsed != strlen(myarray_get(tokens, char *, 1)) || todo < 0) {
        printf("Invalid argument. TODO# must be a positive integer.\n");
        return 0;
    }

    query = sqlite3_mprintf("SELECT COUNT(key) FROM todo WHERE parent = %d;", todo);

    retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    sqlite3_free(query);
    if (retVal != SQLITE_OK) {
        printf("An error occurred while trying to get the data from the database.\n");
        sqlite3_finalize(statement);
        return 1;
    }

    while ((retVal = sqlite3_step(statement)) == SQLITE_ROW) {
        hasChildren = sqlite3_column_int(statement, 0);
    }
    sqlite3_finalize(statement);

    if (hasChildren) {
        printf("This todo has children. Do you want to delete them too?\n");
        printf("Saying no will make these children parentless (toplevel todo's).\n");
        confirmation = readline("yes/no (default is no)? ");

        if (confirmation != NULL && !strncmp(confirmation, "y", 1)) {
            query = sqlite3_mprintf("DELETE FROM todo WHERE parent = %d;", todo);
            sqlite3_exec(db, query, NULL, NULL, &errmsg);
            sqlite3_free(errmsg);
            sqlite3_free(query);
        } else {
            query = sqlite3_mprintf("UPDATE todo SET parent = NULL WHERE parent = %d;\n", todo);
            sqlite3_exec(db, query, NULL, NULL, &errmsg);
            sqlite3_free(errmsg);
            sqlite3_free(query);
        }

        if (confirmation != NULL) free(confirmation);
    }

    query = sqlite3_mprintf("DELETE FROM todo WHERE key = %d;", todo);
    sqlite3_exec(db, query, NULL, NULL, &errmsg);
    sqlite3_free(errmsg);
    sqlite3_free(query);

    return 0;
}
