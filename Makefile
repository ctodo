PROJ=todo
CFLAGS+=-I. -Wall -Werror -ansi
CFLAGS+=`pkg-config --cflags sqlite3`
LDFLAGS+=`pkg-config --libs sqlite3`
LDFLAGS+=-lreadline

OBJS=todo.o add.o del.o help.o list.o mark.o myarray.o quit.o show.o unmark.o

$(PROJ): $(OBJS)
	$(CC) -o $(PROJ) $(OBJS) $(LDFLAGS)

clean:
	rm -rf $(OBJS) $(PROJ)
