#include <show.h>
#include <stdio.h>
#include <time.h>

void show_help() {
    printf("  Usage: show TODO#\n");
    printf("  Shows a single task, without any children.\n");
}

int show(sqlite3 *db, MArray tokens) {
    sqlite3_stmt *statement;
    int retVal;
    int todo;
    int parsed;
    char *query;

    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        show_help();
        return 0;
    } else if (tokens->len != 2) {
        printf("Wrong number of arguments.\n");
        show_help();
        return 0;
    }

    if (sscanf(myarray_get(tokens, char *, 1), "%d%n", &todo, &parsed) < 1 || parsed != strlen(myarray_get(tokens, char *, 1)) || todo < 0) {
        printf("Invalid argument. TODO# must be a positive integer.\n");
        show_help();
        return 0;
    }

    query = sqlite3_mprintf("SELECT * FROM todo WHERE key = %d;", todo);
    retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    sqlite3_free(query);

    if (retVal != SQLITE_OK) {
        printf("Could not perform the query: %s\n", sqlite3_errmsg(db));
    }

    while ((retVal = sqlite3_step(statement)) == SQLITE_ROW) {
        time_t createdDate = sqlite3_column_int(statement, 2);
        time_t dueDate;
        const unsigned char *hasDueDate = sqlite3_column_text(statement, 3);
        int done = sqlite3_column_text(statement, 4) != NULL;

        printf("Todo #%d: %s\n", sqlite3_column_int(statement, 0), sqlite3_column_text(statement, 1));
        printf("Created on: %s", ctime(&createdDate));
        if (hasDueDate) {
            dueDate = sqlite3_column_int(statement, 3);
            printf("Due on: %s", ctime(&dueDate));
        } else {
            printf("No due date.\n");
        }
        printf("Done: %s\n", done ? "yes" : "no");
    }

    sqlite3_finalize(statement);

    return 0;
}
