#include <mark.h>
#include <stdio.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <string.h>

void mark_help() {
    printf("  Usage: mark TODO#\n");
    printf("  Mark the selected TODO# as done.\n");
}

int mark_helper(sqlite3 *db, int todo, int interactive) {
    char *query;
    int retVal;
    int hasUnfinishedTasks = 0;
    char *errmsg;
    sqlite3_stmt *statement;

    query = sqlite3_mprintf("SELECT COUNT(key) FROM todo WHERE parent = %d AND done IS NULL;", todo);
    retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    sqlite3_free(query);

    if (retVal != SQLITE_OK) {
        printf("ERROR: Couldn't perform the query to check if the task has any subtasks that aren't marked as done: %s\n", sqlite3_errmsg(db));
        return 1;
    }

    while (sqlite3_step(statement) == SQLITE_ROW) {
        hasUnfinishedTasks = sqlite3_column_int(statement, 0);
    }

    sqlite3_finalize(statement);

    if (hasUnfinishedTasks) {
        if (interactive) {
            printf("Warning: The selected task has %d unfinished subtasks (not including eventual deeper dependencies). ", hasUnfinishedTasks);
            printf("Marking it as done will also mark all its subtasks as done.\n");
            printf("Do you want to proceed?\n");
            query = readline("yes/no (default is no)? ");
        } else {
            query = malloc(2*sizeof(char));
            query[0] = 'y';
            query[1] = '\0';
        }

        if (query == NULL || strncmp(query, "y", 1)) {
            if (query) free(query);
            return 0;
        } else {
            if (query) free(query);
            query = sqlite3_mprintf("SELECT key FROM todo WHERE parent = %d;", todo);
            retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
            sqlite3_free(query);
            if (retVal != SQLITE_OK) {
                printf("ERROR: Could not mark the subtasks as done: %s\n", sqlite3_errmsg(db));
                return 1;
            }
            while (sqlite3_step(statement) == SQLITE_ROW) {
                mark_helper(db, sqlite3_column_int(statement, 0), 0);
            }
        }
    }

    query = sqlite3_mprintf("UPDATE todo SET DONE = 1 WHERE key = %d;", todo);
    retVal = sqlite3_exec(db, query, NULL, NULL, &errmsg);
    sqlite3_free(query);
    if (retVal != SQLITE_OK) {
        printf("ERROR: Could not mark the task as done: %s\n", errmsg);
        sqlite3_free(errmsg);
        return 1;
    }

    return 0;
}

int mark(sqlite3 *db, MArray tokens) {
    sqlite3_stmt *statement;
    char *query;
    int todo;
    int retVal;
    int parsed;
    int exists = 0;

    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        mark_help();
        return 0;
    } else if (tokens->len != 2) {
        printf("Invalid amount of arguments.\n");
        mark_help();
        return 0;
    }

    if (sscanf(myarray_get(tokens, char *, 1), "%d%n", &todo, &parsed) < 1 || parsed != strlen(myarray_get(tokens, char *, 1)) || todo < 0) {
        printf("Invalid argument. TODO# must be a positive integer.\n");
        mark_help();
        return 0;
    }

    query = sqlite3_mprintf("SELECT COUNT(key) FROM todo WHERE key = %d;", todo);

    retVal = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    sqlite3_free(query);

    if (retVal != SQLITE_OK) {
        printf("ERROR: Couldn't perform the query to check if the task exists: %s\n", sqlite3_errmsg(db));
        return 1;
    }

    while (sqlite3_step(statement) == SQLITE_ROW) {
        exists = sqlite3_column_int(statement, 0);
    }

    sqlite3_finalize(statement);

    if (!exists) {
        printf("The todo you wish to mark as done does not exist.\n");
        return 0;
    }

    return mark_helper(db, todo, 1);
}
