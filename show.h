#ifndef __TODO_SHOW_H
#define __TODO_SHOW_H

#include <myarray.h>
#include <sqlite3.h>

int show(sqlite3 *db, MArray tokens);

#endif
