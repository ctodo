#include <stdio.h>
#include <string.h>
#include <unmark.h>

void unmark_help() {
    printf("  Usage: unmark TODO#\n");
    printf("  Mark the selected TODO# as not done.\n");
}

int unmark(sqlite3 *db, MArray tokens) {
    int retVal = 0;
    if (tokens->len == 2 && !strcmp(myarray_get(tokens, char *, 1), "help")) {
        unmark_help();
    } if (tokens->len == 2) {
        printf("Not yet implemented.\n");
    } else {
        printf("Invalid amount of arguments.\n");
        unmark_help();
    }

    return retVal;
}
