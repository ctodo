#ifndef __TODO_MARK_H
#define __TODO_MARK_H

#include <myarray.h>
#include <sqlite3.h>

int mark(sqlite3 *db, MArray tokens);

#endif
