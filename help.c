#include <help.h>
#include <stdio.h>

int help(sqlite3 *db, MArray tokens) {
    printf("Available commands:\n");
    printf("  add       adds a todo\n");
    printf("  del       deletes a single todo\n");    
    printf("  help      prints this help\n");
    printf("  list      lists all the todos\n");
    printf("  quit      exits the program\n");
    printf("  show      shows a single todo\n");
    printf("  mark      marks a todo as done\n");
    printf("  unmark    marks a todo as not done\n");
    printf("For additional information on a particular command, use '<command> help', e.g. 'list help'.\n");

    return 0;
}
