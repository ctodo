#include <commands.h>
#include <myarray.h>
#include <sqlite3.h>
#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <string.h>

MArray tokenize(char *string, char *delimiter) {
    MArray tokens = myarray_new(5, 5, sizeof(char *));

    char *temp;
    char *substring;
    while ((temp = strstr(string, delimiter)) != NULL) {
        substring = malloc((temp - string + 1) * sizeof(char));
        strncpy(substring, string, temp-string);
        substring[temp-string] = '\0';
        myarray_append(tokens, substring);
        string = temp + strlen(delimiter);
    }

    if (strlen(string)) {
        substring = malloc((strlen(string) + 1) * sizeof(char));
        strcpy(substring, string);
        myarray_append(tokens, substring);
    }

    return tokens;
}

void free_tokens(MArray tokens) {
    int i;
    for (i = 0; i < tokens->len; i++) {
        free(myarray_get(tokens, char *, i));
    }

    myarray_free(tokens);
}

int command_prompt(sqlite3 *db) {
    int retVal = 0;
    char *line;
    char *token;
    MArray tokens;

    line = readline("todo> ");
    add_history(line);
    
    if (!line || !strlen(line)) {
        printf("\n");
        return 1;
    }

    tokens = tokenize(line, " ");
    token = myarray_get(tokens, char *, 0);

    if (!strcmp(token, "quit")) {
        retVal = quit(db, tokens);
    } else if (!strcmp(token, "list")) {
        retVal = list(db, tokens);
    } else if (!strcmp(token, "help")) {
        retVal = help(db, tokens);
    } else if (!strcmp(token, "show")) {
        retVal = show(db, tokens);
    } else if (!strcmp(token, "del")) {
        retVal = del(db, tokens);
    } else if (!strcmp(token, "add")) {
        retVal = add(db, tokens);
    } else if (!strcmp(token, "mark")) {
        retVal = mark(db, tokens);
    } else if (!strcmp(token, "unmark")) {
        retVal = unmark(db, tokens);
    } else {
        printf("Unknown command %s\n", token);
        remove_history(where_history());
    }

    free(line);
    free_tokens(tokens);

    return retVal;
}

int main(int argc, char **argv) {
    int retVal;
    sqlite3 *db;
    char *errmsg;

    sqlite3_initialize();

    retVal = sqlite3_open_v2("todo.db", &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);

    if (retVal != SQLITE_OK) {
        printf("Couldn't open the database file: %s\n", sqlite3_errmsg(db));
    }

    retVal = sqlite3_exec(db,
            "CREATE TABLE IF NOT EXISTS todo \
                (key INTEGER PRIMARY KEY ASC ON CONFLICT ABORT AUTOINCREMENT,\
                 description STRING,\
                 createdDate INTEGER,\
                 dueDate INTEGER DEFAULT NULL,\
                 done INTEGER,\
                 parent INTEGER REFERENCES todo (key) CHECK (key != parent));"
            , NULL, NULL, &errmsg);

    if (retVal != SQLITE_OK) {
        printf("Couldn't create the table for this application: %s\n", errmsg);
        sqlite3_free(errmsg);
        sqlite3_close(db);
        sqlite3_shutdown();
        return 1;
    }

    sqlite3_free(errmsg);

    while (!command_prompt(db)) {};

    sqlite3_close(db);
    sqlite3_shutdown();

    return 0;
}
